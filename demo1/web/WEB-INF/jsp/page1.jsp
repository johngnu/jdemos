<%-- 
    Document   : page1
    Created on : Oct 4, 2017, 1:35:03 PM
    Author     : John Castillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <p>Hola ${nombre}</p>

        <form action="<c:url value="/web/guardar.htm"/>" method="POST">
            <table>
                <tr>
                    <td>Nombre</td>
                    <td>
                        <input name="nombre">
                    </td>
                </tr>
                <tr>
                    <td>Apellido</td>
                    <td>
                        <input name="apellido">
                    </td>
                </tr>
                <tr>
                    <td>Edad</td>
                    <td>
                        <input name="edad">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="submit">Guardar</button>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
