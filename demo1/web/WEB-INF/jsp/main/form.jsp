<%-- 
    Document   : page1
    Created on : Oct 4, 2017, 1:35:03 PM
    Author     : John Castillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulario de registro</title>
    </head>
    <body>
        <h1>Formulario de registro</h1>
        <p>Ingrese los datos correctamente</p>

        <form action="<c:url value="/main/savedata.htm"/>" method="POST">
            <table>
                <tr>
                    <td>Nombre</td>
                    <td>
                        <input name="nombre">
                    </td>
                </tr>
                <tr>
                    <td>Apellido</td>
                    <td>
                        <input name="apellido">
                    </td>
                </tr>
                <tr>
                    <td>Fecha nacimiento <br>
                        (dd/MM/yyyy)</td>
                    <td>
                        <input name="fechaNac">
                    </td>
                </tr>
                <tr>
                    <td>Departamento</td>
                    <td>
                        <select name="departamento">
                            <c:forEach var="d" items="${departamentos}">
                                <option value="${d.codigo}">${d.descripcion}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="submit">Guardar</button>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
