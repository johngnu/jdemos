<%-- 
    Document   : index
    Created on : Oct 4, 2017, 1:35:03 PM
    Author     : John Castillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <h1>Aplicación Spring 4</h1>
        <p>Hello! This is the default welcome page for a Spring Web MVC project.</p>

        <table style="border: black solid 1px">
            <tr>
                <td><strong>Opciones</strong></td>
            </tr>
            <c:forEach var="m" items="${menu}">
                <tr>
                    <td> 
                        <a href="<c:url value="${m.url}"/>">${m.title}</a>
                    </td>
                </tr>
            </c:forEach>
        </table>


    </body>
</html>
