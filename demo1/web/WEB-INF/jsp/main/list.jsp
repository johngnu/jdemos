<%-- 
    Document   : page1
    Created on : Oct 4, 2017, 1:35:03 PM
    Author     : John Castillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado</title>
    </head>
    <body>
        <h1>Listado</h1>
        <p>Registro almacenados en base de datos</p>


        <table>
            <tr>
                <td>Nombre</td>
                <td>Apellido</td>
                <td>Fecha nacimiento</td>
            </tr>
            <c:forEach var="p" items="${personas}">
                <tr>
                    <td>${p.nombre}</td>
                    <td>${p.apellido}</td>
                    <td>${p.fechaNac}</td>
                </tr>
            </c:forEach>
        </table>
    </form>
</body>
</html>
