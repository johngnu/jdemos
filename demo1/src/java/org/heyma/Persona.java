/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.heyma;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author John Castillo
 */
public class Persona {

    private String nombre;
    private String apellido;
    private Integer edad;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fechaNac;

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public void print() {
        System.out.println("nombre: " + this.nombre);
        System.out.println("apellido: " + this.apellido);
        System.out.println("feacha: " + this.fechaNac);
    }
    
}
