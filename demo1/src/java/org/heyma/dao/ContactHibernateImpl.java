/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.heyma.dao;

import java.util.List;
import org.heyma.model.Ciudad;
import org.heyma.model.Contact;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author John Castillo
 */
@Repository
public class ContactHibernateImpl implements ContactDAO {

    @Autowired
    SessionFactory sessionFactory;

    @Transactional
    public void persist(Object entity) {
        this.sessionFactory.getCurrentSession().saveOrUpdate(entity);
    }
    
//    @Transactional
//    @Override
//    public void saveOrUpdate(Contact contact) {
//        this.sessionFactory.getCurrentSession().saveOrUpdate(contact);
//    }

    @Override
    public void delete(int contactId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Contact get(int contactId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true)
    @Override
    public List<Contact> list() {
        Query q = this.sessionFactory.getCurrentSession().createQuery("from Contact");
        return q.list();
    }

//    @Transactional
//    @Override
//    public void gradarCiudad(Ciudad ciudad) {
//        this.sessionFactory.getCurrentSession().saveOrUpdate(ciudad);
//    }
    
    

}
