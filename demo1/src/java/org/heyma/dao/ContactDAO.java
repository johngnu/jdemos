package org.heyma.dao;

import java.util.List;

import org.heyma.model.Contact;

/**
 *
 * @author John Castillo
 */
public interface ContactDAO {

    public void persist(Object obj);
    ///public void saveOrUpdate(Contact contact);

    public void delete(int contactId);

    public Contact get(int contactId);

    public List<Contact> list();
    
    //public void gradarCiudad(Ciudad ciudad);
}
