/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.heyma;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.heyma.dao.ContactDAO;
import org.heyma.model.Departamento;
import org.heyma.model.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author John Castillo
 */
@Controller
@RequestMapping(value = "/main")
public class MainController {

    List<Persona> dataBase = new ArrayList<>();
    @Autowired
    private ContactDAO contactDAO;

    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    public String index(Model model) {
        List<Menu> menu = new ArrayList<>();
        menu.add(new Menu("Formulario de registro", "/main/form.htm"));
        menu.add(new Menu("Listado", "/main/list.htm"));
        
        Menu m = new Menu();
        
        model.addAttribute("menu", menu);
        return "main/index";
    }

    @RequestMapping(value = "/form.htm", method = RequestMethod.GET)
    public String form(Model model) {
        //TODO
        List<Departamento> deps = new ArrayList<>();
        deps.add(new Departamento(1, "La Paz"));
        deps.add(new Departamento(2, "Cochabamba"));
        deps.add(new Departamento(3, "Santa Cruz"));
        
        model.addAttribute("departamentos", deps);
        return "main/form";
    }

    /**
     * Guardar Datos
     *
     * @param p Persona
     * @return html redirect Home page
     */
    @RequestMapping(value = "/savedata.htm", method = RequestMethod.POST)
    public String saveData(Persona p) {
        //TODO
        p.print();
        contactDAO.list();
        
        this.dataBase.add(p);
        return "redirect:index.htm";
    }

    @RequestMapping(value = "/list.htm", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("personas", this.dataBase);
        return "main/list";
    }

}
