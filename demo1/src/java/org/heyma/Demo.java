/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.heyma;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author John Castillo
 */
@Controller
@RequestMapping(value = "/web")
public class Demo {

    private String name;

    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    @ResponseBody
    public String index(Model model) {
        this.setName("John Castillo");
        return "Hola " + this.getName();
    }

    @RequestMapping(value = "/mvc.htm", method = RequestMethod.GET)
    public String mvc(Model model) {
        model.addAttribute("nombre", "John Castillo");
        return "page1";
    }

    @RequestMapping(value = "/guardar.htm", method = RequestMethod.POST)
    public String guardar(Persona per) {
        System.out.println("nombre: " + per.getNombre());
        System.out.println("apellido: " + per.getApellido());
        System.out.println("edad: " + per.getEdad());
        return "page1";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
