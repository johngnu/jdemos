package org.heyma.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.heyma.dao.ContactDAO;
import org.heyma.model.Ciudad;
import org.heyma.model.Contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controlador ejemplo para trabajar con RDBMS PostgreSQL
 *
 * @author John Castillo
 */
@Controller
@RequestMapping(value = "/database")
public class HomeController {

    @Autowired
    private ContactDAO contactDAO;

    @RequestMapping(value = "/crearciudad", method = RequestMethod.GET)
    @ResponseBody
    public String crearCiudad() throws IOException {
       
        Ciudad c = new Ciudad();
        c.setNombre("La Paz");
        contactDAO.persist(c);
        
        return "Ciudad creada...";
    }
    
    
    @RequestMapping(value = "/index")
    public ModelAndView listContact(ModelAndView model) throws IOException {
        List<Contact> listContact = contactDAO.list();
        model.addObject("listContact", listContact);
        model.setViewName("home");
        return model;
    }

    @RequestMapping(value = "/index2", method = RequestMethod.GET)
    public String listContact2(Model model) throws IOException {
        List<Contact> listContact = contactDAO.list();
        model.addAttribute("listContact", listContact);
        return "home2";
    }

    @RequestMapping(value = "/newContact", method = RequestMethod.GET)
    public ModelAndView newContact(ModelAndView model) {
        Contact newContact = new Contact();
        model.addObject("contact", newContact);
        model.setViewName("ContactForm");
        return model;
    }

    @RequestMapping(value = "/saveContact", method = RequestMethod.POST)
    public ModelAndView saveContact(@ModelAttribute Contact contact) {
        contactDAO.persist(contact);
        return new ModelAndView("redirect:/database/index.htm");
    }

    @RequestMapping(value = "/deleteContact", method = RequestMethod.GET)
    public ModelAndView deleteContact(HttpServletRequest request) {
        int contactId = Integer.parseInt(request.getParameter("id"));
        contactDAO.delete(contactId);
        return new ModelAndView("redirect:/database/index.htm");
    }

    @RequestMapping(value = "/editContact", method = RequestMethod.GET)
    public ModelAndView editContact(HttpServletRequest request) {
        int contactId = Integer.parseInt(request.getParameter("id"));
        Contact contact = contactDAO.get(contactId);
        ModelAndView model = new ModelAndView("ContactForm");
        model.addObject("contact", contact);

        return model;
    }
}
